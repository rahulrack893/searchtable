import { Component, OnInit, ViewChild } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { MatPaginator, MatTableDataSource, MatSort } from "@angular/material";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"]
})
export class HomeComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: MatTableDataSource<any>;

  searchId: any;
  page = 1;
  pagesize = 100;
  searchItems = [];
  displayedColumns: string[] = [
    "owner_id",
    "owner_name",
    "owner_profile",
    "creation_date",
    "last_edit_date",
    "answered"
  ];
  constructor(private http: HttpClient) {}

  ngOnInit() {
    this.fatchData();
  }
  fatchData() {
    let apiUrl = `https://api.stackexchange.com/2.2/search/advanced?order=desc&sort=activity&accepted=True&site=stackoverflow&page=${this.page}&pagesize=${this.pagesize}`;

    if (this.searchId) {
      apiUrl += `&user=${this.searchId}`;
    }
    this.http.get(apiUrl).subscribe(res => {
      this.searchItems = JSON.parse(JSON.stringify(res)).items;
      console.log("Count", this.searchItems.length);
      this.dataSource = new MatTableDataSource(this.searchItems);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }
}
